Computer Graphics Project 2 for all platforms
============================================

####Prerequisite
#####For Unix base Systems(Linux, OSX)
1. C++ compile environment
2. [fltk](http://www.fltk.org/software.php)

	Download souce from official site and follow readme instruction.
	
3. build it run and test

for example:
	
	make
	./runmaze

	make buildmaze
	./buildmaze

	make clean
	...


#####For Windows
1. Download [Framwork](http://dgmm.pc-lab.csie.ntust.edu.tw/?ac1=courprojdetail_CG2012F_3&id=52dc82d31e5c9&sid=52dc830201b83) from Website
2. Paste All source code(*.h, *.cpp) to the source directory
3. Set up troublesome environment in VC and switch the Channel to Runmaze, then press `Ctrl` + `F5` to build and run.

####Usage
just clone the git and start coding ^.<~

####Progress
* visible Wall(sadly only one condition)

####Todo List
* Clipping
* Recursive function...


####Others
I only edited the makefile.

You can work on your project in Maze.cpp now.

####[Project Bitbucket Link](https://bitbucket.org/yukaihuang1993/computer-graphics-project-2-maze)